// Simple React Native specific changes

export default {
  // font scaling override - RN default is on
  allowTextFontScaling: true,
  FaircoinEnabled: false,
  CurrencyName: 'Ups',
  AppShortName: '', // Leaving empty since logo contains the name
  WalletEnabled: true,

  // Set to false to disable this buttons from drawer
  howItWorksUrl: 'http://utopoints.com/como-funciona/',
  aboutUsUrl: 'http://utopoints.com/'
}
