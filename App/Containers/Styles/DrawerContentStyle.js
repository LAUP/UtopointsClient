// @flow
import { ApplicationStyles, Colors } from '../../Themes'

export default {
  container: {
    flex: 1,
    backgroundColor: Colors.background,
    padding: 20,
    ...ApplicationStyles.container
  },
  logo: {
    alignSelf: 'center'
  }
}
